# py_mutliplication

Essai de développement d'utilitaires pédagogiques pour l'apprentissage des tables de multiplication.


# Screenshoot
![screenshoot](./screenshoot1.png)
![screenshoot](./screenshoot2.png)

Pour la version GTK

# Dépendances DEBIAN

```
python3-gi
python3-pil
```


## Installation


### Depuis le paquet debian (ne pas tenir compte pour l'instant le paquet n'est pas pret)

L'installer

```$ wget https://framagit.org/CyrilleBiot/nombresGtk/-/blob/main/nombres-gtk_1.0_all.deb```

```# dpkg -i nombres-gtk_1.0_all.deb```

Le supprimer

```# dpkg -i nombres-gtk```


### Depuis le git

Juste cloner et lancer.


```git clone https://framagit.org/CyrilleBiot/py_mutliplication.git```

```./source/gtk-cli-tables-X.py```
