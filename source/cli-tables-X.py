#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import random


#
# You need to enable Emulate terminal in output console setting in the run configurations section of PyCharm to make it compatible with termios.
# To get there, either right click in the file editor then click Modify Run Configuration...
# Or go to the menu bar > Run > Edit Configurations
#
# ===============================================
# fonction inputimeout
# ===============================================
import selectors
import termios
import sys


DEFAULT_TIMEOUT = 30.0
INTERVAL = 0.05

SP = ' '
CR = '\r'
LF = '\n'
CRLF = CR + LF


class TimeoutOccurred(Exception):
    pass


def echo(string):
    sys.stdout.write(string)
    sys.stdout.flush()

def posix_inputimeout(prompt='', timeout=DEFAULT_TIMEOUT):
    echo(prompt)
    sel = selectors.DefaultSelector()
    sel.register(sys.stdin, selectors.EVENT_READ)
    events = sel.select(timeout)

    if events:
        key, _ = events[0]
        return key.fileobj.readline().rstrip(LF)
    else:
        echo(LF)
        termios.tcflush(sys.stdin, termios.TCIFLUSH)
        raise TimeoutOccurred

def win_inputimeout(prompt='', timeout=DEFAULT_TIMEOUT):
    echo(prompt)
    begin = time.monotonic()
    end = begin + timeout
    line = ''

    while time.monotonic() < end:
        if msvcrt.kbhit():
            c = msvcrt.getwche()
            if c in (CR, LF):
                echo(CRLF)
                return line
            if c == '\003':
                raise KeyboardInterrupt
            if c == '\b':
                line = line[:-1]
                cover = SP * len(prompt + line + SP)
                echo(''.join([CR, cover, CR, prompt, line]))
            else:
                line += c
        time.sleep(INTERVAL)

    echo(CRLF)
    raise TimeoutOccurred

inputimeout = posix_inputimeout

#
# ===============================================
# FIN // fonction inputimeout
# ===============================================


score = 0
facteur1  = (1, 10)
facteur2 = (1, 10)



print("Bienvenu(e). Test sur l'ensemble des tables de multiplication.")
prenom = input("Saisis ton prénom : ")

print("Choisis ton niveau :")
print(" ---- 1: Facile")
print(" ---- 2: Moyen")
print(" ---- 3 : Diffcile")

temps = int(input("Niveau de difficulté (1, 2 ou 3) : "))


# Association des niveaux / temps de réponse
# [niveau] [temps de réponse]
rapidite =  [ [0,0],[1,6],[2,4],[3,2] ]


for i in range(10):
    first = (random.randint(facteur1[0], facteur1[1]))
    second = (random.randint(facteur2[0], facteur2[1]))
    message = "{} X {} = ".format(first, second)

    # Saisie du résultat avec chrnomètre
    try:
        reponse = inputimeout(prompt=message, timeout=rapidite[temps][1])
    except TimeoutOccurred:
        reponse = 'Temps imparti dépassé.'

    # Convcersion en integer
    try:
        reponse = int(reponse)
    except ValueError:
        reponse = 0

    # Test de la réponse
    if reponse == first * second:
        score += 1
        print("OK. Score de {} : {} sur {}.".format(prenom, score, i+1))
    else:
        print("NOT OK. Score de {} : {} sur {}.".format(prenom, score, i+1))
