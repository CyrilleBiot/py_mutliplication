#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
gtk-ci-tables-X.py
Logiciel pour l'apprentissage des tables de multiplication
Niveaux Cycle 2, 3, élèves à BEP.
__author__ = "Cyrille BIOT <cyrille@cbiot.fr>"
__copyright__ = "Copyleft"
__credits__ = "Cyrille BIOT <cyrille@cbiot.fr>"
__license__ = "GPL"
__version__ = "0.0"
__date__ = "2022/02/02"
__maintainer__ = "Cyrille BIOT <cyrille@cbiot.fr>"
__email__ = "cyrille@cbiot.fr"
__status__ = "Devel"
"""

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk, GLib
import random, os
from PIL import Image, ImageDraw
class xGtk(Gtk.Window):

    def __init__(self):

        # Quelques variables pour l'environnement
        if os.path.exists('.git'):
            # On est dans le git
            self.dirBase = './source/'
        else:
            if os.path.isdir('/usr/share/tablesMultiplication/') == True:
                # Sur install paquet
                self.dirBase = '/usr/share/tablesMultiplication/'
            else:
                # en dev
                self.dirBase = './'
        print("SELF DIR : ", self.dirBase)


        self.score = 0
        self.tour = 0
        self.facteur1  = (1, 10)
        self.facteur2 = (1, 10)
        self.list = []
        self.tablesNiveau = 0
        self.contrainteTemps = 0
        self.contrainteTempsDuree = 60

        # Creation fenetre
        Gtk.Window.__init__(self, title="Tables de multiplication")
        self.set_default_size(400, 200)
        self.set_border_width(10)
        self.set_resizable(False)
        #self.set_icon_from_file("apropos.png")
        self.set_border_width(10)

        # Pour la capture de la touche <ENTRER>
        self.connect("key-press-event",self.on_key_press_event)

        # Creation GRIB
        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(False)
        self.grid.set_row_homogeneous(False)
        self.grid.set_column_spacing(6)
        self.grid.set_row_spacing(6)

        # Image
        self.creer_image(8,8)

        self.image = self.dirBase + "image.png"
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=self.image, width=150, height=150,
                                                         preserve_aspect_ratio=False)
        self.image = Gtk.Image.new_from_pixbuf(pixbuf)

        # Label CHoix Tables
        self.labelChoixTables = Gtk.Label(label="Choisir sa table")

        # Label multiplication
        self.lblProduit = Gtk.Label(label='Facteur 1 X Facteur 2')
        self.lblProduit.set_name("zoneDeProduit")

        # Entry resultat
        self.entryResultat = Gtk.Entry()
        self.entryResultat.set_text("Résultat")
        self.entryResultat.set_name('zoneDeResultat')

        # Button suivant
        self.btnSuivant = Gtk.Button("Suivant")

        # Label Score
        self.labelScore = Gtk.Label(label="Score")
        self.labelScore.set_name('zoneDeScore')

        # Button Reinitialiser
        self.btnInit = Gtk.Button(label="Accueil")


        # Btn Tables
        self.btnTables = [0,1,2,3,4,5,6,7,8,9]
        self.button_chiffres = [0] * 9

        for i in range(1,10):
            self.btnTables[i] = Gtk.Button(label=i)
            self.btnTables[i].connect('clicked', self.select_tables)


        self.btnAbout = Gtk.Button(label="About")
        self.btnAbout.connect("clicked", self.on_about)

        # Attache sur le GRID ACCUEIL
        self.grid.attach(self.labelChoixTables,0,0,4,1)
        self.grid.attach(self.btnTables[1],0,1,1,1 )
        self.grid.attach(self.btnTables[2],1,1,1,1 )
        self.grid.attach(self.btnTables[3],2,1,1,1 )

        self.grid.attach(self.btnTables[4],0,2,1,1 )
        self.grid.attach(self.btnTables[5],1,2,1,1 )
        self.grid.attach(self.btnTables[6],2,2,1,1 )

        self.grid.attach(self.btnTables[7],0,3,1,1 )
        self.grid.attach(self.btnTables[8],1,3,1,1 )
        self.grid.attach(self.btnTables[9],2,3,1,1 )

        self.btnToutesTables = Gtk.Button(label="Toutes les tables")
        self.btnToutesTables.connect('clicked', self.select_tables)
        self.grid.attach(self.btnToutesTables,0,4,3,1)

        self.grid.attach(self.btnInit, 0, 11, 3, 1)
        self.grid.attach(self.btnAbout,4,11,7,1)

        # Contrainte de temps
        self.btnCheckTemps = Gtk.CheckButton(label="Contrainte temps")
        self.btnCheckTemps.connect("toggled", self.on_button_toggled, "0")

        self.button1 = Gtk.RadioButton.new_with_label_from_widget(None, "Cool (6 sec.)")
        self.button1.connect("toggled", self.on_button_toggled, "60")

        self.button2 = Gtk.RadioButton.new_from_widget(self.button1)
        self.button2.set_label("Normal (4 sec.)")
        self.button2.connect("toggled", self.on_button_toggled, "40")

        self.button3 = Gtk.RadioButton.new_from_widget(self.button1)
        self.button3.set_label("Fais moi mal (2 sec.)")
        self.button3.connect("toggled", self.on_button_toggled, "20")

        # Attache sur page accueil
        self.grid.attach(self.btnCheckTemps, 5, 1, 1, 1)
        self.grid.attach(self.button1,5,2,1,1)
        self.grid.attach(self.button2,5,3,1,1)
        self.grid.attach(self.button3,5,4,1,1)

        # Attache sur le GRID JEU
        self.grid.attach(self.image,0,0,1,7)
        self.grid.attach(self.lblProduit,1,0,1,7)
        self.grid.attach(self.entryResultat,0,7,1,2)
        self.grid.attach(self.btnSuivant,1,7,1,2)
        self.grid.attach(self.labelScore, 0,10,5,1)


        # Les callabcks
        self.btnSuivant.connect("clicked", self.suivant)
        self.btnInit.connect('clicked', self.reinitialiser)

        self.add(self.grid)
        self.show_all()
        self.widgets_accueil('show')


    def reinitialiser(self, btn):
        self.tour = 9999
        self.suivant(btn)
        self.widgets_accueil('show')
        self.score = 0
        self.tour = 0
        self.facteur1  = (1, 10)
        self.facteur2 = (1, 10)
        self.list = []
        self.tablesNiveau = 0
        self.contrainteTemps = 0
        self.contrainteTempsDuree = 60
        self.progressbar.destroy()
        self.btnCheckTemps.set_active(False)
        self.button1.set_active(True)
        self.button2.set_active(False)
        self.button3.set_active(False)




    def on_button_toggled(self, button, name):
        if button.get_active():
            state = "on"
        else:
            state = "off"
        print("Button", name, "was turned", state)

        if self.btnCheckTemps.get_active():
            print("Contrainte temps")
            self.contrainteTemps = 1;
            self.button1.show()
            self.button2.show()
            self.button3.show()
            if self.button1.get_active():
                self.contrainteTempsDuree = 60
            else:
                self.contrainteTempsDuree = int(name)

            print("Dureeé : ", self.contrainteTempsDuree)

        else:
            print("Pas de contrainte")
            self.contrainteTemps = 0;
            self.button1.hide()
            self.button2.hide()
            self.button3.hide()

    def select_tables(self, btn):
        if str(btn.get_label()) in '123456789':
            self.facteur2 = (int(btn.get_label()), int(btn.get_label()))
        self.commencer_jeux(btn)

    def on_timeout(self, user_data):
        if self.tour == 10:
            print("Fini")
        else:
            if self.tour != 9999:
                self.new_value = self.progressbar.get_fraction() + 0.01
                if self.new_value > 1:
                    self.entryResultat.set_text('0')
                    self.suivant(self)
                    self.new_value = 0
                self.progressbar.set_fraction(self.new_value)
                #print(self.new_value)
                return True

    def widgets_accueil (self, state):
        if state == 'show':
            self.image.hide()
            self.lblProduit.hide()
            self.entryResultat.hide()
            self.labelScore.hide()
            self.btnSuivant.hide()
            for i in range(1, 10):
                self.btnTables[i].show()
            self.btnToutesTables.show()
            self.btnCheckTemps.show()
            # Efface le temps
            self.button1.hide()
            self.button2.hide()
            self.button3.hide()
            self.labelChoixTables.show()
        else:
            self.labelChoixTables.hide()
            self.image.show()
            self.lblProduit.show()
            self.entryResultat.show()
            self.labelScore.show()
            self.btnSuivant.show()
            for i in range(1,10):
                self.btnTables[i].hide()
            self.btnToutesTables.hide()
            self.btnCheckTemps.hide()
            self.button1.hide()
            self.button2.hide()
            self.button3.hide()

    def on_key_press_event(self, widget, event):
        # DEBUG
        print("Key press on widget: ", widget)
        print("          Modifiers: ", event.state)
        print("      Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))

        # Si <ENTER> <RETURN> on simule le button SUIVANT
        if Gdk.keyval_name(event.keyval) == "Return":
            self.suivant(self)

    def suivant(self, btn):
        print(self.tour)
        print(type(self.tour))

        if self.tour == 9999:
            print('retour accueil. on coupe tout')
        else:
            if self.tour <= 9:
                self.resultat = int(self.list[self.tour][0]) * int(self.list[self.tour][1])
                print(self.resultat)

                if self.resultat == int(self.entryResultat.get_text()):
                    self.score += 1
                else:
                    pass




                if self.tour == 9:
                    print("partie finie")
                    self.btnSuivant.hide()
                    self.labelScore.set_text('Score : ' + str(self.score) + ' sur ' + str(self.tour))
                    pass

                if self.contrainteTemps == 1:
                    # Réinitialise la progressbar
                    self.progressbar.set_fraction(0)

                self.tour += 1
                print(self.score , " sur " , self.tour)

                self.labelScore.set_text('Score : ' + str(self.score) +  ' sur ' + str(self.tour))
                self.entryResultat.set_text("")
                self.update_image(self.list[self.tour][0], self.list[self.tour][1])
                self.update_label()
                self.entryResultat.grab_focus()



    def update_label(self):
        label = str(self.list[self.tour][0]) + " X " + str(self.list[self.tour][1]) + " ="
        print(label)
        self.lblProduit.set_text(label)

    def commencer_jeux(self, btn):
        self.widgets_accueil("hide")

        # Création d'une série de 10 X
        for i in range(10):
            first = (random.randint(self.facteur1[0], self.facteur1[1]))
            second = (random.randint(self.facteur2[0], self.facteur2[1]))
            self.list.extend([(first,second)])
            #message = "{} X {} = ".format(first, second)

        # DEBUG
        for i_line, line in enumerate(self.list):
            print(i_line, ' : ', line)

        # Nouvelle image
        self.update_label()
        self.update_image(self.list[self.tour][0], self.list[self.tour][1])

        # Initialisation de la zone de saisie
        self.entryResultat.set_text('')
        self.entryResultat.grab_focus()

        if self.contrainteTemps == 1:
            # Pour le Timer
            self.Timeout = self.contrainteTempsDuree
            self.timeout_id = GLib.timeout_add(self.Timeout, self.on_timeout, None)

            # ------ Progress Barr
            self.progressbar = Gtk.ProgressBar()
            self.grid.attach(self.progressbar, 0, 9, 2, 1)
            self.progressbar.show()

            # ------ Start
            self.progressbar.set_fraction(0)

    def creer_image(self, f1, f2)  :
        print("f1 ; " , f1, "/ ", f2 )
        if f1 > 5 and f2 > 5:
            x = 15
            y = 15
        elif f1 < 6 and f2 > 6:
            x = 10
            y = 50
        elif f1 > 6 and f2 < 6:
            x = 50
            y = 10
        else:
            x = 30
            y = 50
        # Fond transparent
        img = Image.new('RGBA', (120, 120), (255, 0, 0,0))

        xbase = x
        l = 10
        draw = ImageDraw.Draw(img)
        # Dessin du manche
        for i in range(f1):
            if (i % 2) == 0:
                backcolor = "yellow"
                pencolor = "black"
            else:
                backcolor = "red"
                pencolor = "black"
            for i in range(f2):
                draw.rectangle([(x, y), (x + l, y + l)], fill=backcolor, outline=pencolor)
                x = x + l
            x = xbase
            y = y + l
        img.save(self.dirBase + 'image.png')

    def update_image(self, f1, f2)  :
        self.creer_image(f1,f2)

        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=self.dirBase + "image.png", width=200, height=200,
                                                         preserve_aspect_ratio=False)
        self.image.set_from_pixbuf(pixbuf)

    def gtk_style(self):
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path(self.dirBase + 'style.css')

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
    def on_about(self, widget):
        """
        Fonction de la Boite de Dialogue About
        :param widget:
        :return:
        """
        # Recuperation n° de version
        print(__doc__)
        lignes = __doc__.split("\n")
        for l in lignes:
            if '__version__' in l:
                version = l[15:-1]
            if '__date__' in l:
                dateGtKBox = l[12:-1]

        authors = ["Cyrille BIOT"]
        documenters = ["Cyrille BIOT"]
        self.dialog = Gtk.AboutDialog()
        logo = GdkPixbuf.Pixbuf.new_from_file(self.dirBase + "apropos.png")
        if logo != None:
            self.dialog.set_logo(logo)
        else:
            print("A GdkPixbuf Error has occurred.")
        self.dialog.set_name("Gtk.AboutDialog")
        self.dialog.set_version(version)
        self.dialog.set_copyright("(C) 2022 Cyrille BIOT")
        self.dialog.set_comments("gtk-cli-tables-X.py.\n\n" \
                                 "[" + dateGtKBox + "]")
        self.dialog.set_license("GNU General Public License (GPL), version 3.\n"
                                "This program is free software: you can redistribute it and/or modify\n"
                                "it under the terms of the GNU General Public License as published by\n"
                                "the Free Software Foundation, either version 3 of the License, or\n"
                                "(at your option) any later version.\n"
                                "\n"
                                "This program is distributed in the hope that it will be useful,\n"
                                "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                                "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                                "GNU General Public License for more details.\n"
                                "You should have received a copy of the GNU General Public License\n"
                                "along with this program.  If not, see <https://www.gnu.org/licenses/>\n")
        self.dialog.set_website("https://cbiot.fr")
        self.dialog.set_website_label("cbiot.fr")
        self.dialog.set_website("https://framagit.org/CyrilleBiot/py_mutliplication")
        self.dialog.set_website_label("GIT ")
        self.dialog.set_authors(authors)
        self.dialog.set_documenters(documenters)
        self.dialog.set_translator_credits("Cyrille BIOT")
        self.dialog.connect("response", self.on_about_reponse)
        self.dialog.run()

    def on_about_reponse(self, dialog, response):
        """
        Fonction fermant la boite de dialogue About
        :param widget:
        :param response:
        :return:
        """
        self.dialog.destroy()





def main():
    """
    La boucle de lancement
    :return:
    """
    win = xGtk()
    win.gtk_style()
    win.move(50,50)
    win.connect("destroy", Gtk.main_quit)
    Gtk.main()
"""
 Boucle main()
"""
if __name__ == "__main__":
    # execute only if run as a script
    main()



